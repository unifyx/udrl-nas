#!/bin/python

import torch
import torch.nn as nn

#============================= LAYERS ===========================#

class LayerLast(nn.Module):
    def __init__(self, in_shape, numOut):
        super().__init__()
        self.in_shape = torch.zeros(in_shape).view(in_shape[0], -1).shape
        self.fc1 = nn.Linear(in_features=self.in_shape[1], out_features=numOut)
        self.activation = nn.LogSoftmax(dim=-1)
    def forward(self, x):
        return self.activation(self.fc1(x.view(x.shape[0], -1)))

class LayerConv(nn.Module):
    def __init__(self, in_shape, parameters):
        super().__init__()

        outChan  = int(parameters[0])
        kernel   = int(parameters[1])
        stride   = int(parameters[2])
        padding  = int(parameters[3])
        dilation = int(parameters[4])
        bias     = int(parameters[5])

        if len(in_shape) == 4:
            self.conv1 = nn.Conv2d(in_shape[1], outChan, kernel, stride, padding, dilation, \
                bias=bool(bias))
        elif len(in_shape) == 3:
            self.conv1 = nn.Conv1d(in_shape[1], outChan, kernel, stride, padding, dilation, \
                bias=bool(bias))
        else:
            self.conv1 = nn.Conv1d(1, outChan, kernel, stride, padding, dilation, \
                bias=bool(bias))

    def forward(self, x):
        if len(x.shape) not in [3, 4]:
            x = x.view(x.shape[0], 1, -1)
        return self.conv1(x)

class LayerLinear(nn.Module):
    def __init__(self, in_shape, parameters):
        super().__init__()

        numOut = int(parameters[0])
        bias   = int(parameters[1])

        self.in_shape = torch.zeros(in_shape).view(in_shape[0], -1).shape

        self.fc1 = nn.Linear(in_features=self.in_shape[1], out_features=numOut, bias=bias)
    def forward(self, x):
        return self.fc1(x.view(x.shape[0], -1))

class LayerMaxPool(nn.Module):
    def __init__(self, in_shape, parameters):
        super().__init__()

        kernel   = int(parameters[0])
        stride   = int(parameters[1])
        padding  = int(parameters[2])
        dilation = int(parameters[3])

        if len(in_shape) == 4:
            self.maxp1 = nn.MaxPool2d(kernel_size=kernel, stride=stride, padding=padding, dilation=dilation)
        else:
            self.maxp1 = nn.MaxPool1d(kernel_size=kernel, stride=stride, padding=padding, dilation=dilation)

    def forward(self, x):
        if len(x.shape) not in [3, 4]:
            x = x.view(x.shape[0], 1, -1)
        return self.maxp1(x)

class LayerDropout(nn.Module):
    def __init__(self, parameters):
        super().__init__()

        prob = parameters[0]

        self.discrete_params = torch.Tensor([prob])
        self.discrete_params = torch.cat((self.discrete_params, torch.zeros(5)), 0)

        self.dropout1 = nn.Dropout(prob)
    def forward(self, x):
        return self.dropout1(x)

class LayerRelu(nn.Module):
    def __init__(self):
        super().__init__()
        self.activation = nn.ReLU()
    def forward(self, x):
        return self.activation(x)

class LayerSigmoid(nn.Module):
    def __init__(self):
        super().__init__()
        self.activation = nn.Sigmoid()
    def forward(self, x):
        return self.activation(x)

class LayerTanh(nn.Module):
    def __init__(self):
        super().__init__()
        self.activation = nn.Tanh()
    def forward(self, x):
        return self.activation(x)

class LayerSoftmax(nn.Module):
    def __init__(self):
        super().__init__()
        self.activation = nn.Softmax(dim=-1)
    def forward(self, x):
        return self.activation(x)

class LayerLogSoftmax(nn.Module):
    def __init__(self):
        super().__init__()
        self.activation = nn.LogSoftmax(dim=-1)
    def forward(self, x):
        return self.activation(x)

class LayerBatchNorm(nn.Module):
    def __init__(self, in_shape):
        super().__init__()
        if len(in_shape) == 4:
            self.batchn1 = nn.BatchNorm2d(in_shape[1])
        elif len(in_shape) == 3:
            self.batchn1 = nn.BatchNorm1d(in_shape[1])
        else:
            self.batchn1 = nn.BatchNorm1d(in_shape[0])
    def forward(self, x):
        if len(x.shape) not in [3, 4]:
            x = x.view(x.shape[0], 1, -1)
        if x.shape[-1] == 1:
            return x
        return self.batchn1(x)

#=========================== GENERATORS =========================#

class GeneratorConv():
    def __init__(self, in_shape, parameters):
        self.in_shape = in_shape

        parameters = list(parameters.detach().numpy())
        self.outChan  = max(round(in_shape[1] * parameters[0] * 4.), 1)
        self.dilation = min(round(1 + parameters[4] * 4), in_shape[-1])
        self.kernel   = max((in_shape[-1] - 1) / self.dilation * parameters[1], 1)
        self.stride   = round(1 + parameters[2] * 16)
        self.padding  = max(round(0.5 * parameters[3] * self.kernel) - 1, 0)
        self.bias     = round(parameters[5])

        self.discrete_params = torch.Tensor([self.outChan / 4., \
                (self.kernel * self.dilation / max(in_shape[-1] - 1, 0.0001)), \
                (self.stride - 1) / 16., (self.padding - 1) * 2. / self.kernel, \
                (self.dilation - 1) / 4., self.bias])
    def getLayer(self):
        return LayerConv(self.in_shape, [self.outChan, self.kernel, self.stride, self.padding, \
                self.dilation, self.bias])

class GeneratorLinear():
    def __init__(self, in_shape, parameters):
        self.in_shape = in_shape

        parameters = list(parameters.detach().numpy())

        self.numOut = round(1 + parameters[0] * 4 * in_shape[-1])
        self.bias   = round(parameters[1])

        self.discrete_params = torch.Tensor([(self.numOut - 1) / 4. / self.in_shape[-1], self.bias])
        self.discrete_params = torch.cat((self.discrete_params, torch.zeros(4)), 0)
    def getLayer(self):
        return LayerLinear(self.in_shape, [self.numOut, self.bias])

class GeneratorMaxPool():
    def __init__(self, in_shape, parameters):
        self.in_shape = in_shape
        parameters = list(parameters.detach().numpy())

        self.dilation = min(round(1 + parameters[3] * 4), in_shape[-1])
        self.kernel   = max((in_shape[-1] - 1) / self.dilation * parameters[0], 1)
        self.stride   = round(1 + parameters[1] * 16)
        self.padding  = max(round(0.5 * parameters[2] * self.kernel) - 1, 0)

        self.discrete_params = torch.Tensor([(self.kernel * self.dilation / max(in_shape[-1] - 1, 0.0001)), \
                (self.stride - 1) / 16., (self.padding - 1) * 2. / self.kernel, \
                (self.dilation - 1) / 4])

        self.discrete_params = torch.cat((self.discrete_params, torch.zeros(2)), 0)
    def getLayer(self):
        return LayerMaxPool(self.in_shape, [self.kernel, self.stride, self.padding, self.dilation])

class GeneratorDropout():
    def __init__(self, parameters):
        parameters = list(parameters.detach().numpy())
        self.prob = parameters[0]

        self.discrete_params = torch.Tensor([self.prob])
        self.discrete_params = torch.cat((self.discrete_params, torch.zeros(5)), 0)
    def getLayer(self):
        return LayerDropout([self.prob])

class GeneratorRelu():
    def __init__(self):
        self.discrete_params = torch.zeros(6)
    @staticmethod
    def getLayer():
        return LayerRelu()

class GeneratorSigmoid():
    def __init__(self):
        self.discrete_params = torch.zeros(6)
    @staticmethod
    def getLayer():
        return LayerSigmoid()

class GeneratorTanh():
    def __init__(self):
        self.discrete_params = torch.zeros(6)
    @staticmethod
    def getLayer():
        return LayerTanh()

class GeneratorSoftmax():
    def __init__(self):
        self.discrete_params = torch.zeros(6)
    @staticmethod
    def getLayer():
        return LayerSoftmax()

class GeneratorLogSoftmax():
    def __init__(self):
        self.discrete_params = torch.zeros(6)
    @staticmethod
    def getLayer():
        return LayerLogSoftmax()

class GeneratorBatchNorm():
    def __init__(self, in_shape):
        self.in_shape = in_shape
        self.discrete_params = torch.zeros(6)
    def getLayer(self):
        return LayerBatchNorm(self.in_shape)
