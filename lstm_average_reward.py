#!/bin/python

import torch
import torch.nn as nn
import torch.optim as optim

from utils import create_net, get_cifar, train_model, delete_logs, mutate_parameters

experiment_name = ''.join(__file__.split('/')[-1].split('.')[:-1])
delete_logs(experiment_name)

max_layers    = 3
meta_batch    = 5
decay_bad     = 0.9995
decay_good    = 0.998
threshold     = 0.1
threshold_inc = 0.001

num_types     = 6
num_params    = 6
layer_size    = num_types + num_params

trainset, testset, image_shape, num_labels = get_cifar(64)
#trainset, testset, image_shape, num_labels = get_mnist(64)

torch.autograd.set_detect_anomaly(True)

class MetaNetwork(nn.Module):
    def __init__(self):
        super().__init__()

        self.sigmoid      = nn.Sigmoid()

        self.lstm1        = nn.LSTMCell(input_size = layer_size, hidden_size=layer_size)

        self.hidden_state = torch.zeros(1, layer_size)
        self.cell_state   = torch.zeros(1, layer_size)
    def reset(self):
        self.hidden_state = torch.zeros(1, layer_size)
        self.cell_state   = torch.zeros(1, layer_size)
    def forward(self, x):
        self.hidden_state, self.cell_state = self.lstm1(x, (self.hidden_state, self.cell_state))

        return self.sigmoid(self.hidden_state)

meta_learner = MetaNetwork()

def get_parameters_with_target(reward, param_targets):
    meta_learner.reset()
    parameters = []

    reward_input = torch.Tensor([[reward for _ in range(layer_size)]])

    for param_target in param_targets:
        if len(parameters) == 0:
            parameters.append(meta_learner(reward_input))
        else:
            parameters.append(meta_learner(param_target))
    return parameters

def get_parameters(reward, uncertainty):
    meta_learner.reset()
    parameters = []

    reward_input = torch.Tensor([[reward for _ in range(layer_size)]])

    for _ in range(max_layers):
        if len(parameters) == 0:
            parameters.append(meta_learner(reward_input))
        else:
            parameters.append(meta_learner(parameters[-1]))
    if uncertainty > 0.:
        for i, _ in enumerate(parameters):
            parameters[i] = parameters[i].detach()
        mutate_parameters(parameters, uncertainty, max_layers, num_types, num_params)
    return parameters


epsilon = 1.0
average_reward = 1.0
meta_criterion = nn.MSELoss()
meta_optimizer = optim.Adam(meta_learner.parameters(), lr = 0.01)
while epsilon > 0.02:
    learners = []

    for _ in range(meta_batch):
        p = get_parameters(min(1., average_reward + 0.1), epsilon)
        learners.append(create_net(p, image_shape, num_types, experiment_name, num_labels))

    results = []
    for model in [x[0] for x in learners]:
        results.append(train_model(model, trainset, testset, experiment_name))

    #learn with results
    targets = []
    outputs = []

    for index, result in enumerate(results):
        average_reward = average_reward + (result - average_reward) / 15

        outputs += get_parameters_with_target(result, learners[index][1])
        targets += learners[index][1]

    targets = torch.stack(targets)
    outputs = torch.stack(outputs)

    meta_optimizer.zero_grad()
    loss = meta_criterion(outputs, targets)
    loss.backward()
    meta_optimizer.step()

    good_results = len([x for x in results if x > threshold])
    bad_results  = meta_batch - good_results
    epsilon     *= (decay_good ** good_results) * (decay_bad ** bad_results)
    threshold   += threshold_inc * good_results
