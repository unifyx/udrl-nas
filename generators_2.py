#!/bin/python

import functools
import torch
import torch.nn as nn

def clip(number):
    return max(min(number, 0.99), 0.01)

#=============================================================================#
#                         _
#                        | | __ _ _   _  ___ _ __ ___
#                        | |/ _` | | | |/ _ \ '__/ __|
#                        | | (_| | |_| |  __/ |  \__ \
#                        |_|\__,_|\__, |\___|_|  |___/
#                                 |___/
#
#=============================================================================#

class LayerLast(nn.Module):
    def __init__(self, in_shape, numOut):
        super().__init__()
        self.in_shape = torch.zeros(in_shape).view(in_shape[0], -1).shape
        self.fc1 = nn.Linear(in_features=self.in_shape[1], out_features=numOut)
        self.activation = nn.LogSoftmax(dim=-1)
    def forward(self, x):
        return self.activation(self.fc1(x.view(x.shape[0], -1)))

class LayerEmpty(nn.Module):
    @staticmethod
    def forward(x):
        return x

class LayerBatchNorm(nn.Module):
    def __init__(self, in_shape):
        super().__init__()
        if len(in_shape) == 5:
            self.batchn1 = nn.BatchNorm3d(in_shape[1])
        if len(in_shape) == 4:
            self.batchn1 = nn.BatchNorm2d(in_shape[1])
        elif len(in_shape) == 3:
            self.batchn1 = nn.BatchNorm1d(in_shape[1])
        else:
            raise ValueError("BatchNorm not applicable after linear layer")
    def forward(self, x):
        return self.batchn1(x)

class LayerConv(nn.Module):
    def __init__(self, in_shape, parameters):
        super().__init__()

        self.relu = nn.ReLU()

        outChan   = int(parameters[0])
        kernel    = int(parameters[1])
        stride    = int(parameters[2])
        padding   = int(parameters[3])
        dilation  = int(parameters[4])
        bias      = int(parameters[5])

        if len(in_shape) == 5:
            self.conv1 = nn.Conv3d(in_shape[1], outChan, kernel, stride, padding, dilation, \
                bias=bool(bias))
        if len(in_shape) == 4:
            self.conv1 = nn.Conv2d(in_shape[1], outChan, kernel, stride, padding, dilation, \
                bias=bool(bias))
        elif len(in_shape) == 3:
            self.conv1 = nn.Conv1d(in_shape[1], outChan, kernel, stride, padding, dilation, \
                bias=bool(bias))
        else:
            raise ValueError("Convolution not applicable after linear layer")

    def forward(self, x):
        return self.relu(self.conv1(x))

class LayerLinear(nn.Module):
    def __init__(self, in_shape, parameters):
        super().__init__()

        self.relu = nn.ReLU()

        numOut    = int(parameters[0])
        bias      = int(parameters[1])

        self.in_shape = torch.zeros(in_shape).view(in_shape[0], -1).shape

        self.fc1 = nn.Linear(in_features=self.in_shape[1], out_features=numOut, bias=bias)
    def forward(self, x):
        return self.relu(self.fc1(x.view(x.shape[0], -1)))

class LayerMaxPool(nn.Module):
    def __init__(self, in_shape, parameters):
        super().__init__()

        self.relu = nn.ReLU()

        kernel    = int(parameters[0])
        stride    = int(parameters[1])
        padding   = int(parameters[2])
        dilation  = int(parameters[3])

        if len(in_shape) == 5:
            self.maxp1 = nn.MaxPool3d(kernel_size=kernel, stride=stride, \
                    padding=padding, dilation=dilation)
        if len(in_shape) == 4:
            self.maxp1 = nn.MaxPool2d(kernel_size=kernel, stride=stride, \
                    padding=padding, dilation=dilation)
        elif len(in_shape) == 3:
            self.maxp1 = nn.MaxPool1d(kernel_size=kernel, stride=stride, \
                    padding=padding, dilation=dilation)
        else:
            raise ValueError("MaxPool not applicable after linear layer")

    def forward(self, x):
        return self.relu(self.maxp1(x))

class LayerDropout(nn.Module):
    def __init__(self, parameters):
        super().__init__()

        self.relu = nn.ReLU()

        prob = parameters[0]

        self.discrete_params = torch.Tensor([prob])
        self.discrete_params = torch.cat((self.discrete_params, torch.zeros(5)), 0)

        self.dropout1 = nn.Dropout(prob)
    def forward(self, x):
        return self.relu(self.dropout1(x))

#=============================================================================#
#
#                                              _
#               __ _  ___ _ __   ___ _ __ __ _| |_ ___  _ __ ___
#              / _` |/ _ \ '_ \ / _ \ '__/ _` | __/ _ \| '__/ __|
#             | (_| |  __/ | | |  __/ | | (_| | || (_) | |  \__ \
#              \__, |\___|_| |_|\___|_|  \__,_|\__\___/|_|  |___/
#              |___/
#
#=============================================================================#

class GeneratorConv():
    def __init__(self, in_shape, parameters):
        self.in_shape = in_shape

        parameters = list(parameters.detach().numpy())
        self.outChan  = max(round(in_shape[1] * parameters[0] * 4.), 1)
        self.dilation = round(1 + parameters[4] * 4)
        self.kernel   = max((in_shape[-1] - 1) / self.dilation * parameters[1], 1)
        self.stride   = round(1 + parameters[2] * 4)
        self.padding  = max(round(0.5 * parameters[3] * self.kernel) - 1, 0)
        self.bias     = round(parameters[5])

        self.discrete_params = torch.Tensor([\
                clip(self.outChan / 4.), \
                clip(self.kernel * self.dilation / max(in_shape[-1] - 1, 0.0001)), \
                (self.stride - 1) / 16., \
                clip((self.padding + 1) * 2. / self.kernel), \
                clip((self.dilation - 1) / 4.), \
                self.bias])
    def getLayer(self):
        return LayerConv(self.in_shape, [self.outChan, self.kernel, self.stride, self.padding, \
                self.dilation, self.bias])

class GeneratorLinear():
    def __init__(self, in_shape, parameters):
        self.in_shape = in_shape

        parameters = list(parameters.detach().numpy())

        num_in = functools.reduce(lambda x,y:x*y, in_shape[1:])

        discrete_param = int(parameters[0] * 16) / 16
        self.numOut = round(1 + discrete_param * 4 * num_in)
        self.bias   = round(parameters[1])

        if self.numOut * num_in > 4e6:
            raise ValueError(f"Number of parameters too large({self.numOut} * {num_in} = {self.numOut * num_in})")

        self.discrete_params = torch.Tensor([discrete_param, self.bias])
        self.discrete_params = torch.cat((self.discrete_params, torch.zeros(4)), 0)
    def getLayer(self):
        return LayerLinear(self.in_shape, [self.numOut, self.bias])

class GeneratorMaxPool():
    def __init__(self, in_shape, parameters):
        self.in_shape = in_shape
        parameters = list(parameters.detach().numpy())

        self.dilation = round(1 + parameters[3] * 4.)
        self.kernel   = max((in_shape[-1] - 1) / self.dilation * parameters[0], 1)
        self.stride   = round(1 + parameters[1] * 4.)
        self.padding  = max(round(0.5 * parameters[2] * self.kernel) - 1, 0)

        self.discrete_params = \
                torch.Tensor([\
                clip(self.kernel * self.dilation / max(in_shape[-1] - 1, 0.0001)), \
                (self.stride - 1) / 16., \
                clip((self.padding + 1) * 2. / self.kernel), \
                clip((self.dilation - 1) / 4)])

        self.discrete_params = torch.cat((self.discrete_params, torch.zeros(2)), 0)
    def getLayer(self):
        return LayerMaxPool(self.in_shape, [self.kernel, self.stride, self.padding, self.dilation])

class GeneratorDropout():
    def __init__(self, parameters):
        parameters = list(parameters.detach().numpy())
        self.prob = (int(parameters[0] - 0.001 * 9) + 1)/10

        self.discrete_params = torch.Tensor([(self.prob - 0.1) / 0.9])
        self.discrete_params = torch.cat((self.discrete_params, torch.zeros(5)), 0)
    def getLayer(self):
        return LayerDropout([self.prob])

class GeneratorBatchNorm():
    def __init__(self, in_shape):
        self.in_shape = in_shape
        self.discrete_params = torch.zeros(6)
    def getLayer(self):
        return LayerBatchNorm(self.in_shape)

class GeneratorEmpty():
    def __init__(self):
        self.discrete_params = torch.zeros(6)
    def getLayer(self):
        return LayerEmpty()
