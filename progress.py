import time
import os

class Progress:
    def __init__(self, description):
        self.description = description
        self.start = time.clock_gettime(0)
        self.active = True
        try:
            self.columns, _ = os.get_terminal_size(1)
            self.columns -= 15 + len(self.description) + 2
        except:
            self.active = False

    def update(self, status):
        if not self.active:
            return
        now = time.clock_gettime(0)
        timediff = now - self.start
        if status != 0:
            eta = int(timediff / status - timediff)
        else:
            eta = 0
        barstring = self.description + ': ' + str(int(status * 100)).rjust(3) + \
                '% [' + '#' * int(status * self.columns) + '-' * \
                (self.columns - int(status * self.columns)) + ']'
        print('\r' + barstring + ' ' + self.getTimeString(eta), end='', sep='')

    def end(self):
        if not self.active:
            return
        barstring = self.description + ': ' + '100% [' + '#' * self.columns + ']'
        print('\r' + barstring + ' ' + self.getTimeString(time.clock_gettime(0) - self.start), \
                sep='')

    @staticmethod
    def getTimeString(seconds):
        hours = int(seconds / 3600)
        minutes = int(seconds % 3600 / 60)
        seconds = int(seconds % 60)

        return str(hours) + ':' + str(minutes).zfill(2) + ':' + str(seconds).zfill(2)
